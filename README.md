# ![Développement (Logiciel) Durable](img/maintenabilite-screenshot.png)

Une présentation sur les techniques de programmation améliorant la [maintenabilité](http://en.wikipedia.org/wiki/Maintainability)
des applications. Vous pouvez consulter différentes version en ligne via [l'historique](#historique).

## Installation

 1. `git clone --recursive https://gitlab.com/lcottereau/maintainability-slides.git` va récupérer le source du projet, ainsi que la bibliothèque [reveal.js](https://github.com/hakimel/reveal.js)
 1. Ouvrir le fichier [index.html](index.html) dans votre navigateur. Il vaut mieux le faire via http pour avoir toutes les fonctionnalités de reveal, en particulier les notes du présentateur (touche `s`).

Note : vous pouvez aussi installer à partir de [l'archive à télécharger](https://gitlab.com/lcottereau/maintainability-slides/repository/archive.zip?ref=master),
mais dans ce cas-là il vous faudra aller chercher séparément la bibliothèque [reveal.js](https://github.com/hakimel/reveal.js/archive/master.zip) et la copier dans le
répertoire `reveal` à la racine du projet.

## Historique

* 8 novembre 2023 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20231108/)
* 13 novembre 2020 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20201113/)
* 22 janvier 2020 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20200122/)
* 8 novembre 2019 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20191108/)
* 26 novembre 2018 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20181126/)
* 19 novembre 2018 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20181119/)
* 20 novembre 2017 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20171120/)
* 13 novembre 2017 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20171113/)
* 12 décembre 2016 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20161205/)
* 21 novembre 2016 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20161121/)
* 16 novembre 2015 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20151116/)
* 12 octobre 2015 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20151012/)
* 16 avril 2015 : [License PRO CSID de l'IUT de Montreuil (Paris 8)](https://lcottereau.gitlab.io/maintainability-slides/20150416/)
* 8 décembre 2014 : [Option informatique de l'INSA de Lyon](http://lcottereau.gitlab.io/maintainability-slides/20141208/)
* 1<sup>er</sup> décembre 2014 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20141201/)
* 23 mai 2014 : [License PRO CSID de l'IUT de Montreuil (Paris 8)](http://lcottereau.gitlab.io/maintainability-slides/20140523/)
* 20 mai 2014 : [IUT Agile 2014 à Montreuil](http://lcottereau.gitlab.io/maintainability-slides/20140520/) (version de 2h)
* 28 octobre 2013 : [Option ASR de Telecom SudParis](http://lcottereau.gitlab.io/maintainability-slides/20131028/)
* 22 mars 2013 : [License Pro CSID de l'IUT de Montreuil (Paris 8)](http://lcottereau.gitlab.com/maintainability-slides/20130322/)

## Contexte

Si vous souhaitez utiliser ce cours, il faut déterminer les points suivants :

* sur quel projet est-il possible de se baser (exemples, TPs). Idéalement, ce doit être un projet de groupe
* niveau de matûrité en java et sur les différentes briques techniques évoquées (en particulier si on parle usine de dev)
* matériel disponible (rétroprojecteur, PCs ou portables pour les TPs)

Vous pouvez par ailleurs rechercher les éléments `class="specific"` dans la présentation pour personnaliser la
présentation en fonction des projets utilisés.

## Durées (hors exercices)

* Introduction : 10 à 15 minutes
* 1 - Less Is More : 50 à 60 minutes (TP à 5-10 minutes de la fin)
* PAUSE
* 2 - WTF : 20 minutes
* 3 - Votre code est votre documentation : 20 minutes (TP à la fin)
* DEJEUNER
* 4 - Prenez vos responsabilités : 35-40 minutes
* Usine de développement : 25 minutes (TP à la fin)
* PAUSE
* Environnements : 20 minutes (TP à la fin)
* Conclusion : 5 minutes
